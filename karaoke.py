#!/usr/bin/python3
# -- coding: utf-8 --

from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import smallsmilhandler
import sys
import json
import urllib.request


class KaraokeLocal:

    def __init__(self, fich):

        parser = make_parser()
        cHandler = smallsmilhandler.SmallSMILHandler()
        parser.setContentHandler(cHandler)
        parser.parse(fich)
        self.my_list = cHandler.get_tags()

    def __str__(self):

        resultado = ""
        for sublista in self.my_list:
            etiquetas = sublista[0]
            atributos = sublista[1]
            resultado += str(etiquetas + '\t')
            for clave in atributos:
                if atributos[clave]:
                    resultado += (str(clave + '="' + atributos[clave] + '"' + '\t'))
            resultado += "\n"
        return resultado

    def to_json(self, fich_name=""):

        if not fich_name:
            fich_name = "local.json"
        else:
            fich_name = fich_name.replace(".smil", ".json")
        with open(fich_name, 'w') as archivo_json:
            json.dump(self.my_list, archivo_json, indent=4,
                      separators=(' ', ':'), sort_keys=True)

    def do_local(self):
        for sublista in self.my_list:
            atributos = sublista[1]
            for clave in atributos:
                if atributos[clave].startswith('http://'):
                    urllib.request.urlretrieve(atributos[clave],
                                               atributos[clave].split('/')[-1])
                    atributos[clave] = atributos[clave].split('/')[-1]


if __name__ == "__main__":
    try:
        fich = str(sys.argv[1])

    except IndexError:
        print("Usage:  Python3 karaope.py file.smil")
    list = KaraokeLocal(fich)
    print(list)
    list.to_json(fich)
    list.do_local()
    list.to_json()
    print(list)